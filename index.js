var timerWatch = new Stopwatch(function () {
	$('#timer').text(timerWatch.toString());
}, 100);
var rateStarted = false;
var rateStart = 0;


$(document).ready(function() {
	$('#split-calc-fm').submit(splitCalc);
	$('#stopwatch-fm').submit(function () { return false; });
	timerWatch.setListener(function () {
		$('#timer').text(timerWatch.toString());
	});
	$('#lbutton').on("vclick", leftClick);
	$('#rbutton').on("vclick", rightClick);
	$('#rateButton').on("vclick", rateClick);
});

function leftClick() {
	if (timerWatch.started) {

	} else {
		$('#lbutton').text("Split").button('refresh');
		$('#rbutton').text("Stop").button('refresh');
		timerWatch.start();
		timerRunning = true;
	}
	
}

function rightClick() {
	if (timerWatch.started) {
		timerWatch.stop();
		$('#lbutton').text("Start").button('refresh');
		$('#rbutton').text("Reset").button('refresh');
	} else {
		timerWatch.reset();
		$('#timer').text(timerWatch.toString());
	}
	
}

function rateClick() {
	if (rateStarted) {
		rateStop = new Date().getTime();
		rateElapsed = rateStop - rateStart;
		rateStart = new Date().getTime();
		$('#rate').text(parseInt(180000/rateElapsed));
	} else {
		rateStart = new Date().getTime();
		rateStarted = true;
	}
	return false;
}


function splitCalc() {
	mode = $('input[name=radio-calc]:checked').val();
	switch (mode) {
		case "split": 
			timeArray = $('#input-time').val().split(":");
			timeSeconds = parseInt(timeArray[0])*60+parseInt(timeArray[1]);
			splitSeconds = 500 * timeSeconds / parseInt($('#input-dist').val());
			splitStrMinutes = String(Math.floor(splitSeconds/60))
			splitStrRemainder = String((splitSeconds%60).toFixed(0))
			if(splitStrRemainder==60) {
				splitStrRemainder="00";
				splitStrMinutes = String(parseInt(splitStrMinutes)+1);
				}
			if(splitStrRemainder.length==1) {
				splitStrRemainder = '0'+splitStrRemainder;
			}
			splitString = splitStrMinutes+":"+String(splitStrRemainder);
			$('#input-split').val(splitString);
			break;
		case "dist":
			timeArray = $('#input-time').val().split(":");
			timeSeconds = parseInt(timeArray[0])*60+parseInt(timeArray[1]);
			splitArray = $('#input-split').val().split(":");
			splitSeconds = parseInt(splitArray[0])*60+parseInt(splitArray[1]);
			distanceString = String((500*timeSeconds/splitSeconds).toFixed(0));
			$('#input-dist').val(distanceString);
			break;
		case "time":
			splitArray = $('#input-split').val().split(":");
			splitSeconds = parseInt(splitArray[0])*60+parseInt(splitArray[1]);
			timeSeconds = splitSeconds * $('#input-dist').val()/500;
			timeStrMinutes = String(Math.floor(timeSeconds/60))
			timeStrRemainder = String((timeSeconds%60).toFixed(0))
			if(timeStrRemainder==60) {
				timeStrRemainder="00";
				timeStrMinutes = String(parseInt(timeStrMinutes)+1);
				}
			if(timeStrRemainder.length==1) {
				timeStrRemainder = '0'+timeStrRemainder;
			}
			timeString = timeStrMinutes+":"+String(timeStrRemainder);
			$('#input-time').val(timeString);
			break;
	}
	return false;
	
}